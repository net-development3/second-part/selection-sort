﻿using System;

// ReSharper disable InconsistentNaming
#pragma warning disable SA1611

namespace SelectionSort
{
    public static class Sorter
    {
        public static void SelectionSort(this int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int smallestInd, temp;

            for (int i = 0; i < array.Length - 1; i++)
            {
                smallestInd = i;

                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[j] < array[smallestInd])
                    {
                        smallestInd = j;
                    }
                }

                temp = array[smallestInd];
                array[smallestInd] = array[i];
                array[i] = temp;
            }
        }

        public static void RecursiveSelectionSort(this int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            Sorting(array, 0);
        }

        public static int FindMinInd(int[] array, int firstIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (firstIndex == array.Length - 1)
            {
                return firstIndex;
            }

            int othersMinIndex = FindMinInd(array, firstIndex + 1);

            if (array[firstIndex] < array[othersMinIndex])
            {
                return firstIndex;
            }
            else
            {
                return othersMinIndex;
            }
        }

        public static void Sorting(int[] array, int i)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array));
            }

            if (i == array.Length)
            {
                return;
            }

            int minIndex = FindMinInd(array, i);

            if (minIndex != i)
            {
                int temp = array[minIndex];
                array[minIndex] = array[i];
                array[i] = temp;
            }

            Sorting(array, i + 1);
        }
    }
}
